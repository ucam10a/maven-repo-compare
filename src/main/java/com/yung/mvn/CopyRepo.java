package com.yung.mvn;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.yung.tool.FileUtil;

public class CopyRepo {

    public static void copyRepo(List<String> compareRepoList, File baseRepo, File outDir) throws Exception {
        List<String> baseList = CreateLibTxt.getMavenRepoList(baseRepo);
        for (String lib : baseList) {
            if (compareRepoList.contains(lib)) {
                continue;
            }
            List<String> fileList = new ArrayList<String>();
            File node = FileUtil.getFile(baseRepo.getAbsolutePath() + "/" + lib);
            FileUtil.generateFileList(node, fileList);
            for (String copyFilePath : fileList) {
                File copyFile = FileUtil.getFile(outDir.getAbsolutePath() + "/" + lib + "/" + copyFilePath);
                FileUtil.generateDir(copyFile.getAbsolutePath());
                File srcFile = FileUtil.getFile(baseRepo.getAbsolutePath() + "/" + lib + "/" + copyFilePath);
                FileUtil.fileCopy(srcFile, copyFile.getAbsolutePath());
            }
        }
    }
    
    
    public static void main(String args[]) throws Exception {
        
        String base = "E:/git/bitbucket/maven-repo/base";
        File baseRepo = FileUtil.getFile(base);
        
        String out = "E:/git/bitbucket/maven-repo/out";
        File outRepo = FileUtil.getFile(out);
        
        String test = "E:/git/bitbucket/maven-repo/test";
        File testRepo = FileUtil.getFile(test);
        List<String> compareRepoList = CreateLibTxt.getMavenRepoList(testRepo);
        
        CopyRepo.copyRepo(compareRepoList, baseRepo, outRepo);
        
    }
    
}
