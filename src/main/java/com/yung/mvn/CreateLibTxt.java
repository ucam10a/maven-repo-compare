package com.yung.mvn;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import com.yung.tool.FileUtil;
import com.yung.tool.MarshalHelper;

public class CreateLibTxt {

    public static List<String> readLines(File file, String encoding) throws Exception {
        List<String> result = new ArrayList<String>();
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), encoding));
        try {
            String line = br.readLine();
            while (line != null) {
                if (line != null && !line.equals("")) {
                    result.add(line);
                }
                line = br.readLine();
            }
        } finally {
            br.close();
        }
        return result;
    }
    
    public static void writeToFile(File file, String fileText, String encoding) {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), encoding));
            writer.write(fileText);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Close the writer regardless of what happens...
                writer.close();
            } catch (Exception e) {
            }
        }
    }
    
    public static List<String> getMavenRepoList(File file) throws Exception {
        List<String> result = new ArrayList<String>();
        TreeSet<String> temp = new TreeSet<String>();
        List<String> fileList = new ArrayList<String>();
        List<String> patterns = new ArrayList<String>();
        patterns.add("pom");
        patterns.add("jar");
        patterns.add("ear");
        patterns.add("war");
        FileUtil.generateFileList(file, fileList, patterns);
        for (String f : fileList) {
            int idx = f.lastIndexOf("/");
            temp.add(f.substring(0, idx));
        }
        result.addAll(temp);
        return result;
    }
    
    public static void main(String args[]) throws Exception {
        
        String maven_repo = "C:/Users/long/.m2/repository";
        File base = FileUtil.getFile(maven_repo);
        List<String> repoList = getMavenRepoList(base);
        
        String fileFolder = "E:/test";
        String filename = "libs";
        MarshalHelper helper = new MarshalHelper();
        helper.marshal(fileFolder, filename, "repoList", repoList);
    
    }
    
}
